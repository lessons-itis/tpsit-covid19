# 4AI e 5AI covid19

TODO: aggiornare il README con il contenuto della issue #1 tenendo comunque le informaizoni utili qui sotto.



L'obiettivo è di sviluppare una dashboard (un cruscotto) in PHP per poter visualizzare le informazioni aggiornate sulla diffusione del coronavirus #covid19

Questa pagina visualizzerà le informazioni in forma tabulare e in forma di mappa.

### dove prendiamo i dati?

La [AGID - Agenzia per l'Italia Digitale](https://www.agid.gov.it/) / la Protezione Civile italiana hanno creato un [repository Github contenente di dati di diffusione del coronavirus](https://github.com/pcm-dpc/COVID-19) aggiornati ogni giorno

In questo sito è possibile scaricare i file aggiornati in formato CSV, o JSON suddiviso per regioni/province

### vogliamo visualizzare le informazioni per regione/provincia/o cosa?

Le province sono più vicine a noi

### preferiamo CSV o JSON?
Gli sviluppatori in genere preferiscono JSON, ma noi inizieremo dal CSV
* [https://guidaphp.it/base/gestione-file/csv](https://guidaphp.it/base/gestione-file/csv)
* [https://guidaphp.it/base/gestione-file/json](https://guidaphp.it/base/gestione-file/json)

### recupereremo i dati direttamente da github.com oppure sincronizzeremo i file statici sul nostro server?

In una prima fase recupereremo i files direttamente online ([ad esempio dati province](https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv)).

Questo però vuol dire che ogni volta che un utente accede alla nostra pagina PHP il sistema farà una richiesta HTTP all'host raw.githubusercontent.com con conseguenti problemi di carico (ma potremo passarci sopra considerando il traffico di Github.com), e di latenza (lentezza nella risposta, cosa invece molto fastidiosa per l'utente).

Quindi si prevede di attivare un meccanismo di sincronizzazione tra gli Open Data pubblicati e il server che servirà la pagina PHP (semplice da implementare tramite una `git pull`), o in alternativa un meccanismo di cache che richieda al server raw.githubusercontent.come i dati la prima volta, e si salvi sul server la risposta.

Quest'ultimo meccanismo è più complesso e non si pensa di implementarlo.

## Struttura del repository

TODO: questa struttura del repository non è totalmente sbagliata, ma era precedente alla issue #1

* index.php: conterrà la visualizzazione ritenuta migliore e il link alle altre visualizzazioni
* table.php: conterrà la visualizzazione in forma tabulare
* map.php: conterrà la visualizzazione su mappa

## Province visualizzate

In una prima versione si visualizzeranno le province delle Marche

## Possibili sviluppi

* Predisporre filtro lato client (javascript)
* Predisporre filtri lato server (da inserire nel querystring)
* Pubblicare il sito all'interno del portale della scuola
* Salvare solo i dati che servono su database
* Dare la possibilità di accedere tramite account e trovare i propri filtri personalizzati
* Procedura automatica di deploy degli aggiornamenti
