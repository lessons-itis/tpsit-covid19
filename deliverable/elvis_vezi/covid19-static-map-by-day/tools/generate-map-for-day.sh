#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv

dateY=$(date +"%Y");
datem=$(date +"%m");
dated=$(date +"%d");
date=$(echo "$dateY-$datem-$dated")

cat map-head.html

cat dpc-covid19-ita-province.csv |grep$1| grep Marche | while read line; do

lat=$(echo "$line" | cut -f8 -d ',');
lon=$(echo "$line" | cut -f9 -d ',');
casi=$(echo "$line" | cut -f10 -d ',');


echo "marker = L.marker([$lat,$lon]).addTo(mymap);"
echo "marker.bindPopup(\"<b>casi $casi</b>\");"
done

cat map-foot.html

rm  dpc-covid19-ita-province.csv*
