#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv

cat map-head.html

cat dpc-covid19-ita-province.csv | grep "Marche" | while read var;do

	lat=$(echo "$var" | cut -f8 -d ',');
	lon=$(echo "$var" | cut -f9 -d ',');
	reg=$(echo "$var" | cut -f10 -d ',');
	prov=$(echo "$var" | cut -f6 -d ',');
	
	echo "marker = L.marker([$lat,$lon]).addTo(mymap);"
	echo "marker.bindPopup(\"<b>regione $reg provincia $prov</b>\");"
done

cat map-foot.html

rm  dpc-covid19-ita-province.csv*
