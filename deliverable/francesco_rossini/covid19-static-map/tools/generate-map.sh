#!/bin/sh

cat map covid19_map-head.html
curl https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv -O

cat dpc-covid19-ita-province.csv |grep 'Marche'|cut -d',' -f 3-2 |while read line; 
do
	lat=$(echo "$line" | cut -f6 -d',');
	lon=$(echo "$line" | cut -f7 -d',');
	ncasi=$(echo "$line" | cut -f2 -d',');
	echo "marker = L.marker([$lat, $lon]).addTo(mymap);"
	echo "marker.bindPopup(\"<b> casi $ncasi</b>\");"
done
cat map-foot.html

rm dpc-covid19-ita-province.csv*
