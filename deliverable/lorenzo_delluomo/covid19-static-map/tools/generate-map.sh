#!/bin/sh


curl https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv -O

cat map-head.html

cat dpc-covid19-ita-province.csv |grep 'Marche'|cut -d',' -f 6-10 |while read line; 
do
	lat=$(echo "$line" | cut -f3 -d',');
	lon=$(echo "$line" | cut -f4 -d',');
	ncasi=$(echo "$line" | cut -f5 -d',');
	echo "marker = L.marker([$lat, $lon]).addTo(mymap);"
	echo "marker.bindPopup(\"<b> casi $ncasi</b>\");"
done
cat map-foot.html

rm dpc-covid19-ita-province.csv*
