#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv

# mi creo tre variabili anno, mese e giorno della data corrente
dateY=$(date +"%Y");
datem=$(date +"%m");
dated=$(date +"%d");
# queste tre variabili le assegno ad una quarta per il formato
date=$(echo "$dateY-$datem-$dated")

cat map-head.html

cat dpc-covid19-ita-province.csv | grep $(echo $date) | while read line; do
	lat=$(echo "$line" | cut -f8 -d',');
	lon=$(echo "$line" | cut -f9 -d',');
	ncasi=$(echo "$line" | cut -f10 -d',');
	provincia=$(echo "$line" | cut -f6 -d',');
	denprovincia=$(echo "$line" | cut -f7 -d',');
	echo "marker = L.marker([$lat, $lon]).addTo(mymap);"
	echo "marker.bindPopup(\"<b>$provincia($denprovincia)  casi: $ncasi   date $date</b>\");"
done




cat map-foot.html


rm dpc-covid19-ita-province.csv*

