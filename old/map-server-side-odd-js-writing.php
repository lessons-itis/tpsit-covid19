<html>
<head>
    <title>Visualizzazione su mappa coronavirus per province</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
<!-- Make sure you put this AFTER Leaflet's CSS -->
 <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
   integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
   crossorigin=""></script>
<style type="text/css">
    #mapid { height: 700px; }
</style>

</head>
<body>

<section class="section">
    <div class="container">
        <div id="mapid"></div>
    </div>
</section>

<script>
var mymap = L.map('mapid').setView([51.505, -0.09], 6);

L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	subdomains: 'abcd',
	minZoom: 1,
	maxZoom: 16,
	ext: 'jpg'
}).addTo(mymap);

<?php
/* Questo codice PHP scrive tutto il javascript necessario per visualizzare i marker su una mappa
 * La mappa è realizzata con la libreria Leaflet che vedete importata sopra.
 * PRO di questa soluzione:
 *   viene creata una pagina statica con del Javascript che può essere facilmente salvata in un file .html
 *   e quindi distribuita anche come allegato se vogliamo. Non ha dipendenze
 * CONTRO di questa soluzione:
 *  non si sfrutta la dinamicità di un ipertesto che ci consentirebbe di fare filtri o di rendere la mappa più interattiva
 */

// Step 1: Viene recuperato il file remoto CSV dei dati covid19 del giorno
// Se non trovato, viene richiesto il file del giorno precedente

$mydate = date("Ymd");
$url = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province-".$mydate.".csv";
$handler = fopen($url, "r");
if (!$handler) {
    //try yesterday
    $mydate=date("Ymd", time() - 60*60*24);
    $url = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province-".$mydate.".csv";
    $handler = fopen($url, "r");
}

// Step 2: Scorrendo una per una le righe del CSV ottenuto
// (ogni riga viene messa come lista nella variabile $data grazie alla funzione fgetcsv)
// il PHP scrive il codice Javascript necessrio a piazzare tutti i marker.
$c = 0;
while($data = fgetcsv($handler)) {
    if ($c) {
        if ((int)$data[9] > 0) {
            echo "marker = L.marker([".$data[7].", ".$data[8]."]).addTo(mymap);\n";
            $popup="<b>".$data[9]." casi</b>";
            echo "marker.bindPopup(\"$popup\");\n";
        }
    }
    $c += 1;
};
fclose($handler);
?>
</script>
</body>
</html>
