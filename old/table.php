<html>
<head>
    <title>Visualizzazione tabulare coronavirus per province</title>
</head>
<body>

<?php
        // $url = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv";
        // $url = "dpc-covid19-ita-province.csv";

        $url = "covid19.csv";
        $handler = fopen($url, "r");
        $c = 0;
        while($data = fgetcsv($handler)) {
            if (!$c) { ?>

                <table>
                <thead>
                    <tr>
                        <?php
                            foreach ($data as $cell) {
                                echo "<th>".$cell."</th>";
                            }
                        ?>
                    </tr>
                </thead>
                <tbody>

            <?php
            } else {
                echo "<tr>";
                foreach ($data as $cell) {
                    echo "<td>".$cell."</td>";
                }
                echo "</tr>\n";
            };
            $c += 1;
        };
        fclose($handler);
    ?>
    </tbody>
    </table>
</body>
</html>
