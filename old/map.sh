#!/bin/bash/

# curl https://raw.githubusercontent.com/pcm-dcp/COVID-19/master/dati-province/$

cat map-head.html

cat dpc-covid19-ita-province.csv| grep '2020-03-22'| while read line; do
        lat=$(echo "$line" | cut -f8 -d',');
        lon=$(echo "$line" | cut -f9 -d',');
        ncasi=$(echo "$line" | cut -f10 -d',');
        data=$(echo "$line" | cut -f1-2 -d',');
        # echo $line
        # echo "$ncasi casi sono stati rilevati alle coordinate lon=$lon lat=$l$
        echo"marker = L.marker([$lat, $lon ]).addTo(mymap);"
        echo"marker.bindPopup(\"<b>$ncasi, $data casi,data</b>\");"

done

cat map-foot.html
