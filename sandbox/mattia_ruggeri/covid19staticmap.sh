#!/bin/sh

cat map-covid19_map-head.html
curl https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv -O

cat dpc-covid19-ita-province.csv | grep 'Marche'| while read line; do
        lat=$(echo "$line" | cut -f8 -d',');
        lon=$(echo "$line" | cut -f9 -d',');
        numcasi=$(echo "$line" | cut -f10 -d',');
                echo $line
            echo "$numcasi casi sono stati rilevati alle coordinate lat=$lat lon=$lon";
           echo "marker = L.marker([$lat,$lon]).addTo(mymap);"
           echo "marker.bindPopup(\"<b>casi $numcasi casi</b>\");"


done
cat map-covid19_map-foot.html
rm dpc-covid19-ita-province.csv*
#  map covid.sh
#  
#
#  Created by Mattia Ruggeri on 21/03/2020.
#  
