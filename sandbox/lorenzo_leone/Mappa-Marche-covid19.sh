#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv

cat map-head.html

cat dpc-covid19-ita-province.csv | grep 'Marche' | cut -d',' -f8-10 | while read line; do

	#if [ date eq (cat dpc-covid19-ita-province.csv | grep 'Marche' | cut -d',' -f1)]; then
	
	lat=$(echo "$line" | cut -f1 -d',');
	lon=$(echo "$line" | cut -f2 -d',');
	ncasi=$(echo "$line" | cut -f3 -d',');
	echo "marker = L.marker([$lat, $lon]).addTo(mymap);"
	echo "marker.bindPopup(\"<b>$ncasi casi</b>\");"
	
	#fi
	
done

rm dpc-covid19-ita-province.csv*