#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/Covid-19/master/dati-province/dpc-covid19-ita-province.csv

cat map-head.html

if [ -z "$1" ]; then

	echo "passa la data come parametro per Anno, Mese, Giorno"

else
	cat dpc-covid19-ita-province.csv | grep $1| while read line; do

		lat=$(echo "$line" | cut -f8 -d ',');
		lon=$(echo "$line" | cut -f9 -d ',');
		casi=$(echo "$line" | cut -f10 -d ',');
		province=$(echo "$line" | cut -f6 -d ',');

		echo "marker = L.marker([$lat,$lon]).addTo(mymap);"
		echo "maker.bindPopup(\"<b>provincia $province, casi $casi</b>\");"
	done

fi

cat map-foot.html
rm dpc-covid19-ita-province.csv*