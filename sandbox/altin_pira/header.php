<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <!-- Titolo sitoweb -->
        <title>Covid-19</title>
        <!-- css della mappa in uso -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
        <!-- script della mappa -->
        <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

        <!-- Larghezza mappa -->
        <style type="text/css">
          #mapid { height: 700px; }
        </style>

    </head>
  <body>
    <script>
        document.addEventListener('DOMContentLoaded', function () {

          // Get all "navbar-burger" elements
          var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

          // Check if there are any navbar burgers
          if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
              $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

              });
            });
          }

        });
    </script>

    <nav role="navigation" aria-label="main navigation" class="navbar is-primary">
        <div class="navbar-brand">
            <a class="navbar-item" href="/"> Covid-19</a>
            <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="my_menu">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
        </div>
        <div class="navbar-menu" id="my_menu">
            <a class="navbar-item" href="login.php">Home</a>
            <a class="navbar-item" href="login-minimal.php">Home minimale</a>
            <a class="navbar-item" href="client.php">Comunica la tua posizione</a>
            <a class="navbar-item" href="map.php">Mappa</a>
            <a class="navbar-item" href="dashboard.php">Tabella</a>
            <a class="navbar-item" href="dashboard-ajax.php">Tabella (AJAX)</a>
            <a class="navbar-item" href="logout.php">Logout</a>
        </div>
    </nav>
