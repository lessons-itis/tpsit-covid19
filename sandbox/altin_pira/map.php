<?
  require "header.php"
?>


<section class="section">
    <div class="container">
        <div id="mapid"></div>
    </div>
</section>

<script>
var mymap = L.map('mapid').setView([42, 12], 6);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	subdomains: 'abcd',
  id: 'mapbox/streets-v11',
	minZoom: 1,
	maxZoom: 16,
	ext: 'jpg'
}).addTo(mymap);

<?php
/* Questo codice PHP scrive tutto il javascript necessario per visualizzare i marker su una mappa
 * La mappa è realizzata con la libreria Leaflet che vedete importata sopra.
 * PRO di questa soluzione:
 *   viene creata una pagina statica con del Javascript che può essere facilmente salvata in un file .html
 *   e quindi distribuita anche come allegato se vogliamo. Non ha dipendenze
 * CONTRO di questa soluzione:
 *  non si sfrutta la dinamicità di un ipertesto che ci consentirebbe di fare filtri o di rendere la mappa più interattiva
 */

// Step 1: Viene recuperato il file remoto CSV dei dati covid19 del giorno
// Se non trovato, viene richiesto il file del giorno precedente

/*$mydate = date("Ymd");
$url = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province-".$mydate.".csv";*/
$url = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province-latest.csv";
$handler = fopen($url, "r");

// Step 2: Scorrendo una per una le righe del CSV ottenuto
// (ogni riga viene messa come lista nella variabile $data grazie alla funzione fgetcsv)
// il PHP scrive il codice Javascript necessrio a piazzare tutti i marker.
$c = 0;
while($data = fgetcsv($handler)) {
    if ($c) {
        if ((int)$data[9] > 0) {
            echo "marker = L.marker([".$data[7].", ".$data[8]."]).addTo(mymap);\n";
            $popup="<b>".$data[9]." casi</b>";
            echo "marker.bindPopup(\"$popup\");\n";
        }
    }
    $c += 1;
};
fclose($handler);
?>
</script>

<?
  require "footer.html";
?>
