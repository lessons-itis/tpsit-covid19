#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv

cat map-head.html

cat dpc-covid19-ita-province.csv | while read riga;do

	lat=$(echo "$riga" | cut -f8 -d ',');
	lon=$(echo "$riga" | cut -f9 -d ',');
	contagi=$(echo  "$riga" | cut -f10 -d ',');
	echo "marker = L.marker([$lat,$lon]).addTo(mymap);"
	echo "marker.bindPopup(\"<b>contagi $contagi</b>\");"
done

cat map-foot.html

rm dpc-covid19-ita-province.csv

