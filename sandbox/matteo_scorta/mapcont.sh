#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv

dateY=$(date +"%Y");
datem=$(date +"%m");
dated=$(date +"%d");
date=$(echo "$dateY-$datem-$dated")

null=0



if [ -z "$1" ]; then
	cat dpc-covid19-ita-province.csv | grep $(echo $date) | while read line; do
		lat=$(echo "$line" | cut -f11 -d',');
		lon=$(echo "$line" | cut -f12 -d',');
		ncasi=$(echo "$line" | cut -f13 -d',');
		provincia=$(echo "$line" | cut -f6 -d',');
		echo "marker = L.marker([$lat, $lon]).addTo(mymap);"
		echo "marker.bindPopup(\"<b>$provincia  casi $ncasi date $date</b>\");"
	done
else
	cat dpc-covid19-ita-province.csv | grep $1 | while read line; do
		lat=$(echo "$line" | cut -f11 -d',');
		lon=$(echo "$line" | cut -f12 -d',');
		ncasi=$(echo "$line" | cut -f13 -d',');
		provincia=$(echo "$line" | cut -f10 -d',');
		echo "marker = L.marker([$lat, $lon]).addTo(mymap);"
		echo "marker.bindPopup(\"<b>$provincia  casi $ncasi date $1</b>\");"
	done
fi



cat map-foot.html


rm dpc-covid19-ita-province.csv*
