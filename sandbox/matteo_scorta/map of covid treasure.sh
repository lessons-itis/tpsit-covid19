#!/bin/bash

wget https://raw.githubusercontent.com/pcm-dpc/Covid-19/master/dati-province/dpc-covid19-ita-province.csv

cat map-head.html

if [ -z "$1" ]; then

	echo "passare la data come parametro nel seguente formato: YYYY-MM-DD"

else
	cat dpc-covid19-ita-province.csv | grep $1| while read line; do

		lat=$(echo "$line" | cut -f11 -d ',');
		lon=$(echo "$line" | cut -f12 -d ',');
		casi=$(echo "$line" | cut -f13 -d ',');
		province=$(echo "$line" | cut -f10 -d ',');

		echo "marker = L.marker([$lat,$lon]).addTo(mymap);"
		echo "maker.bindPopup(\"<b>casi $casi provincia $province</b>\");"
	done
fi

cat map-foot.html

rm dpc-covid19-ita-province.csv*
