#!/bin/bash

# Step 1. Per ogni riga del file alunni.txt
cat alunni.txt | while read utente; do

	# Step 2. Aggiungo un utente con 
	# username = contenuto della riga
	echo "Sto aggiungendo l'utente: $utente";
	useradd --create-home $utente;
	echo "Aggiunto utente: $utente";

done

# Usati:
# - ciclo while
# - variable substitution
# - metodo base per sperimentare con gli script

# Visti:
# - Utenti nel file /etc/passwd
# - Home degli utenti nella directory /home/

# Metodo:
#
# 1. creare il file vuoto
# 2. inserire la prima riga #!/bin/bash
# 3. scrivere i commenti in modo 'algoritmico' (step by step)
# 4. iniziare ad implementare fino a fare una prova banale di esecuzione
# 5. prima di eseguire lo script formulare un'ipotesi di output
# 6. eseguire lo script e osservare l'output
# 7. se l'output è quello che mi aspettavo -> sono contento e vado avanti
# 8. se l'output non è quello che mi aspettavo -> sono contento e 
#	ragiono/faccio i un'altra prova banale e ripeto dal punto 5.




