#!/bin/bash

# Questo script crea le directory per ogni utente
# Prima di eseguirlo sono stati lanciati questi comandi da riga di comando.
#  mv * old/
#  mv old/README.md .
#  ls
#  ls old
#  git add .
#  git commit -m "Spostata la struttura vecchia in old/"
#  mkdir covid19-static-map
#  mkdir covid19-static-map/tools
#  mkdir -p covid19-static-map-by-day/tools
#  mkdir covid19-php-to-html-and-js
#  mkdir covid19-php-rest-backend-and-js-ajax-jquery ; mkdir sandbox ; mkdir deliverable


cat nome_cognome4A nome_cognome.txt | while read student; do
    dirsandbox="sandbox/$student";
    dirdeliverable="deliverable/$student";

    mkdir "$dirsandbox";
    touch "$dirsandbox/.gitkeep";
    mkdir "$dirdeliverable";
    touch "$dirdeliverable/.gitkeep";

    # Copia la struttura delle directory principali
    # in ogni directory repository dello studente
    for dirinside in covid19-php-rest-backend-and-js-ajax-jquery covid19-static-map covid19-php-to-html-and-js covid19-static-map-by-day; do
        cp -r $dirinside $dirdeliverable
    done


done
